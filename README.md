<img src="assets/kgol-banner.png">

## What is the K-Gol Platform?

**K-Gol Platform** is a collection of components composing a [Golo(*)](https://golo-lang.org/) Serverless platform deployed on Kubernetes:

- 🐳 **K-Gol-Runtime**: a container image to initialize a Golo Pod. The Golo Pod will execute the Golo function.
- 🚀 **K-Gol-Deploy**: a kubectl plugin to seamless deploy Golo Functions to Kubernetes
- 🎁 **K-Gol-Template**: a sample to quickly bootstrap a Golo Function
- 🛠 **K-Gol-Tools**: a container image of tools dedicated to CI

> (*): **Golo is a lightweight dynamic language for the JVM, built from day 1 with invokedynamic**

![alt video is loading ⏳](assets/kgol-demo.gif)

## What is Golo?

Eclipse Golo started out of the experiments from the [Dynamid](https://dynamid.citi-lab.fr/) research team at [CITI-Inria laboratory](http://www.citi-lab.fr/) and [INSA-Lyon](https://www.insa-lyon.fr/). While Golo is indeed *"yet another JVM language"*, it has interesting applications for tinkerers as a standalone language, as an embedded language into other JVM applications, or even for quick prototyping in IoT settings.

Golo is now a Technology Project at the Eclipse Foundation, and a friendly community is taking care of its evolution.

## Getting Started with K-Gol in 4 steps

**1-** Initialize k-gol project from the official template

```bash 
git clone git@gitlab.com:k-gol-platform/k-gol-template.git k-gol-hello
cd k-gol-hello
export SUB_DOMAIN=wey-yu-cluster.k3s.nimbo
```

**2-** Write your function:

```python
function goloFunc = |args| {
  return "👋 Hello Golo"
}
```

**3-** Commit and Deploy:

```bash
git add .
git commit -m "🎉 1st commit"
kubectl kgol deploy
```

**4-** Call your Golo function:

```bash
curl http://k-gol-hello.master.wey-yu-cluster.k3s.nimbo/exec
```

That's all:

```bash
HTTP/1.1 200 OK
Content-Length: 53
Content-Type: application/json;charset=UTF-8
Date: Tue, 31 Mar 2020 08:40:13 GMT
Vary: Accept-Encoding

{
    "result": "👋 Hello Golo"
}

```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md)





